<?php
//Часть I - Управление

//Посчитанная матрица для вращения (45 градусов за одно нажатие)
$direction_x = array(
    0,
    10,
    10,
    10,
    0, -10, -10, -10
);
$direction_y = array(
    10,
    10,
    0, -10, -10, -10,
    0,
    10
);

//Проверим установлены ли переменные 
if (!isset($a) && !isset($b))
{
    $a = 0;
    $b = 0;
}

if (!isset($x) && !isset($y))
{
    $x = 0;
    $y = 10;
    $i = - 1;
}

//Управление. Вращение  влево и вправо
$i = $_GET['i'];

if (isset($_GET['rotate_clockwise']))
{
    if ($i == 7) $i = - 1;
    $i++;
    $x = $direction_x[$i];
    $y = $direction_y[$i];
}

if (isset($_GET['rotate_counter_clockwise']))
{
    if ($i == 0) $i = 8;
    $i--;
    $x = $direction_x[$i];
    $y = $direction_y[$i];
}

//Управление. Движение вперёд. Только вперёд! : )
if (isset($_GET['forward']))
{
    if ($_GET['i'] == '') $i = 0;
    $x = $_GET['x'];
    $y = $_GET['y'];

    $a = $_GET['a'] + ($direction_x[$i]);
    $b = $_GET['b'] + ($direction_y[$i]);

}
else
{
    $a = $_GET['a'];
    $b = $_GET['b'];

}




for ($h=0; $h!=150; $h+=5) {
	print '<div id="wall" style="left: '.(300+$h).'px;"></div>';
    $map[]=300+$h;
}


//Часть II - Бросание лучей

//FOV - определяет угол зрения. 
$fov=0.2618;

//Рейкастинг - бросание лучей. j - хранит  количество лучей - это определяет поле зрения (FOV).
	for ($j=0; $j != 40; $j++) {
		
//Вычисляем угол следующего луча. Отнимаем 1.5 градуса. FOV - field of view - поле обзора.
$fov-=0.02618;		
$x1= floor($x*cos(0.2618+$fov)-$y*sin(0.2618+$fov));
$y1= floor($x*sin(0.2618+$fov)+$y*cos(0.2618+$fov));


//Векторы
$vecx=$x1;
$vecy=$y1;


//Бросаем луч. k - влияет на длину луча
	for ($k=0; $k != 60; $k++) {		
	$vecx+=$x1/10;
	$vecy+=$y1/10;
	//Рисуем лучи синим цветом
print '<div id="light_of_rays" style="height: 10px; top: '.($vecx+210+$a).'px; left: '.($vecy+310+$b). 'px;"></div>';

if (in_array($vecy+310+$b, $map) == $vecy+310+$b) {
$left=array_search($vecy+310+$b, $map);	



//Эмуляция пересечений с объектом и вывод. 
   if (floor($vecx+210+$a) == 200 && floor($vecy+310+$b) == $map[$left] )
print '<div id="wall_real" style="height: '.(100-$k-$j*(cos(x))).'px; top: '.(20+$k).'px; left: '.($map[$left]).'px;"></div>';
      if (ceil($vecx+210+$a) == 200 && ceil($vecy+310+$b) == $map[$left] )
print '<div id="wall_real" style="height: '.(100-$k-$j*(cos(x))).'px; top: '.(20+$k).'px; left: '.($map[$left]).'px;"></div>';
         if (ceil($vecx+210+$a) == 200 && floor($vecy+310+$b) == $map[$left] )
print '<div id="wall_real" style="height: '.(100-$k-$j*(cos(x))).'px; top: '.(20+$k).'px; left: '.($map[$left]).'px;"></div>';
            if (floor($vecx+210+$a) == 200 && ceil($vecy+310+$b) == $map[$left] )
print '<div id="wall_real" style="height: '.(100-$k-$j*(cos(x))).'px; top: '.(20+$k).'px; left: '.($map[$left]).'px;"></div>';
		
		}
	}
}

?>
<style>
			#point_of_view {
			position: absolute;
			top:  <?=$x + 210 + $a ?>px;
			left: <?=$y + 310 + $b ?>px;
			background-color: #999;
			width: 5px;
			height: 5px;
			}	
		
			#point_zero {
			position: absolute;
			top:  <?=$a + 210 ?>px;
			left: <?=$b + 310 ?>px;
			background-color: #333;
			width: 5px;
			height: 5px;
			}
			
			#wall {
			position: absolute;
			top:  200px;
			left: 300px;
			background-color: green;
			width: 5px;
			height: 5px;
			}
			#light_of_rays {
			position: absolute;
			background-color: blue;
			width: 5px;
			height: 5px;
			}
			
			#wall_real {
			position: absolute;
			bottom:  315px;
			left: 30px;
			background-color: green;
			width:  10px;
			height: 10px;
			}		
</style>


<div id="wall">
</div>
<div id="point_of_view">
</div>
<div id="point_zero">
</div>


<form  method="GET" align="left">
<input type="submit" name="rotate_clockwise"  value="rotate_clockwise">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
</form>

<form  method="GET" align="left">
<input type="submit" name="rotate_counter_clockwise"  value="rotate_counter_clockwise">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
</form>

<form  method="GET" >
<input type="submit" name="forward" value="move">
<input type="hidden" name="a" value="<?=$a ?>">
<input type="hidden" name="b" value="<?=$b ?>">
<input type="hidden" name="x" value="<?=$x ?>">
<input type="hidden" name="y" value="<?=$y ?>">
<input type="hidden" name="i" value="<?=$i ?>">
</form>
